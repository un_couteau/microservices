init: docker-down-clear docker-pull docker-build docker-up node-init
down: docker-down-clear
restart: down init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

node-init:
	docker-compose run --rm node-cli yarn install

migrate-up:
	docker-compose run --rm node-cli yarn run migrate